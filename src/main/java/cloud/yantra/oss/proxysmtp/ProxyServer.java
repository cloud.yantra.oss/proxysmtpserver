package cloud.yantra.oss.proxysmtp;

import com.icegreen.greenmail.store.*;
import com.icegreen.greenmail.util.*;
import com.sun.mail.util.*;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.*;
import java.security.*;
import java.util.*;
import java.util.logging.*;

public class ProxyServer {
    private static final Logger LOGGER = Logger.getLogger(ProxyServer.class.getName());

    public static void main(String[] args) {

        if (args.length < 1) {
            LOGGER.severe("Provide the property file containing the upstream details.");
            System.exit(-1);
        }
        String PROPERTIES_FILE = args[0];

        try {
            // Load test email addresses from properties file
            Properties proxyProperties = new Properties();
            proxyProperties.load(new FileInputStream(PROPERTIES_FILE));
            String PROXY_SMTP_SERVER_BIND_ADDRESS = proxyProperties.getProperty("proxy.server.bind-address", "0.0.0.0");
            String PROXY_SMTP_SERVER_AUTHENTICATION_USER = proxyProperties.getProperty("proxy.server.authentication.user", "user");
            String PROXY_SMTP_SERVER_AUTHENTICATION_PASSWORD = proxyProperties.getProperty("proxy.server.authentication.password", "");
            int PROXY_SMTP_SERVER_PORT = Integer.parseInt(proxyProperties.getProperty("proxy.server.port", "3025"));
            // The wait period is read as seconds, and converted to milliseconds
            long PROXY_EMAIL_WAIT_PERIOD = Integer.parseInt(proxyProperties.getProperty("proxy.incoming.email.wait", "5")) * 1000L;
            int PROXY_EMAIL_PROCESSING_COUNT = Integer.parseInt(proxyProperties.getProperty("proxy.incoming.email.processing-count", "1"));
            String UPSTREAM_SMTP_SERVER = proxyProperties.getProperty("smtp.server.url", "smtp.gmail.com");
            int UPSTREAM_SMTP_PORT = Integer.parseInt(proxyProperties.getProperty("smtp.server.port", "25"));
            String UPSTREAM_SMTP_PROTOCOL = proxyProperties.getProperty("smtp.server.protocol", ServerSetup.PROTOCOL_SMTP);
            Boolean UPSTREAM_SMTP_MAIL_DEBUG = Boolean.parseBoolean(proxyProperties.getProperty("smtp.server.debug", "false"));
            String UPSTREAM_SMTP_USERNAME = proxyProperties.getProperty("smtp.server.username", "example-user");
            String UPSTREAM_SMTP_PASSWORD = proxyProperties.getProperty("smtp.server.password", "example-password");
            String TEST_SENDER_EMAIL = proxyProperties.getProperty("test.sender.email", "dummy@example.com");
            String recipientsList = proxyProperties.getProperty("test.recipients.emails", "dummy@example.com");
            // Parse using standard: https://www.w3.org/Protocols/rfc822/
            InternetAddress[] testRecipients = InternetAddress.parse(recipientsList);

            LOGGER.info("The Proxy server will listen on port:" + PROXY_SMTP_SERVER_PORT);
            LOGGER.info("The Upstream SMTP server url:" + UPSTREAM_SMTP_SERVER);
            LOGGER.info("The Upstream SMTP server port:" + UPSTREAM_SMTP_PORT);
            LOGGER.info("The Upstream SMTP protocol:" + UPSTREAM_SMTP_PROTOCOL);
            LOGGER.info("The Upstream SMTP server username:" + UPSTREAM_SMTP_USERNAME);
            LOGGER.info("The list of email recipients to which the emails will be forwarded:\n" + InternetAddress.toString(testRecipients));

            // Set up SSL socket factory for connecting to Upstream SMTP server
            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);

            // Connect to Upstream SMTP server using SMTP over TLS
            Properties smtpProps = new Properties();
            smtpProps.put("mail.smtp.host", UPSTREAM_SMTP_SERVER);
            smtpProps.put("mail.smtp.port", UPSTREAM_SMTP_PORT);
            smtpProps.put("mail.smtp.auth", "true");
            smtpProps.put("mail.smtp.starttls.enable", "true");
            smtpProps.put("mail.smtp.starttls.required", "true");
            smtpProps.put("mail.smtp.socketFactory.port", UPSTREAM_SMTP_PORT);
            smtpProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            smtpProps.put("mail.debug", UPSTREAM_SMTP_MAIL_DEBUG);

            // Create a GreenMail server setup for SMTP protocol
            ServerSetup smtpSetup = new ServerSetup(PROXY_SMTP_SERVER_PORT, PROXY_SMTP_SERVER_BIND_ADDRESS, UPSTREAM_SMTP_PROTOCOL);
            ;
            // Start the GreenMail server
            GreenMail greenMail = new GreenMail(smtpSetup);
            greenMail.start();

            // Add user is provided
            greenMail.setUser(PROXY_SMTP_SERVER_AUTHENTICATION_USER, PROXY_SMTP_SERVER_AUTHENTICATION_PASSWORD);

            // For each incoming email, send it to test users and then forward it to super.prod.com
            // Continue waiting for messages. One need to kill the program to stop listening to messages.
            while (true) {
                greenMail.waitForIncomingEmail(PROXY_EMAIL_WAIT_PERIOD, PROXY_EMAIL_PROCESSING_COUNT);

                // Get the received message
                MimeMessage[] receivedMessages = greenMail.getReceivedMessages();
                if (receivedMessages.length == 0) {
                    LOGGER.info("No messages received for " + PROXY_EMAIL_WAIT_PERIOD / 1000 + " seconds.");
                    continue;
                } else {
                    LOGGER.info("Total number of emails waiting to be processed is [" + receivedMessages.length + "].");
                }

                for (MimeMessage msg : receivedMessages) {
                    LOGGER.info("Received message from " + msg.getFrom()[0] + " to " + msg.getAllRecipients()[0] + ": " + msg.getSubject());
                    LOGGER.fine("Received message from " + msg.getFrom()[0] + " to " + msg.getAllRecipients()[0] + ": " + msg.getSubject() + " with content:\n" + msg.getContent());
                    // Replace original recipients with test emails
                    // Send to test users
                    javax.mail.Session session = javax.mail.Session.getInstance(smtpProps, new javax.mail.Authenticator() {
                        protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                            return new javax.mail.PasswordAuthentication(UPSTREAM_SMTP_USERNAME, UPSTREAM_SMTP_PASSWORD);
                        }
                    });
                    // Create a completely New message, for sending forward
                    Message smsg = new MimeMessage(session);
                    smsg.setFrom(new InternetAddress(TEST_SENDER_EMAIL));
                    smsg.setRecipients(Message.RecipientType.TO, testRecipients);
                    smsg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(TEST_SENDER_EMAIL));
                    smsg.setSubject(msg.getSubject());
                    smsg.setContent(msg.getContent(), msg.getContentType());

                    Transport.send(smsg);
                    LOGGER.info("Email sent with subject:" + msg.getSubject());
                    break; // Force to process only 1 message. Until resolution of bug is found.
                }
                // Empty the mailbox
                greenMail.purgeEmailFromAllMailboxes();
            }
        } catch (IOException | MessagingException | GeneralSecurityException | FolderException e) {
            LOGGER.severe(e.getLocalizedMessage());
            LOGGER.severe(e.getMessage());
            e.printStackTrace();
        }

    }
}
