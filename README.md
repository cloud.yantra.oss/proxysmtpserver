# Proxy SMTP Server

## Building Executable Jar

Execute the below command to build an executable jar.
```shell
mvn clean package
```

This will produce a jar named `proxy-smtp-server-1.0-SNAPSHOT-shaded.jar` in the directory `target/`. 
This will be a fat jar containing all required dependencies.

The file [sample.properties](src/main/resources/sample.properties) contains an example entries for all keys supported. 
You should take it as reference and create an property file that works for you.

You can execute the below command to run the proxy SMTP server.
```shell
java -jar target/proxy-smtp-server-1.0-SNAPSHOT-shaded.jar /path/to/your/property-file
``` 

Happy Mocking SMTP!
